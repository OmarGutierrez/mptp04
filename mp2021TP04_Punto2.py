import os

def continuar():
    print()
    input('Ingrese una tecla para continuar ...')
    os.system('cls')

def menu():
    print(' -------------------MENU----------------------')
    print('1) REGISTRAR PRODUCTOS')
    print('2) MOSTRAR PRODUCTOS.')
    print('3) MOSTRAR PRODUCTOS SEGUN EL INTERVALO [a,b] DE STOCK')
    print('4) SUMAR X STOCK A TODO STOCK < Y')
    print('5) ELIMINAR PRODUCTO CON STOCK 0. ')
    print('6) SALIR.....')
    print('------------------------------------------------')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        print('ERROR .....')
        eleccion = int(input('Error...Elija una opción valida: '))
    os.system('cls')
    return eleccion

def leerPrecio():
    print('........$PRECIO$........')
    precio = float(input('ingrese precio: $'))
    while not(precio > 0):
        print('error...ingrese un precio positivo')
        precio =float(input('ingrese precio: $'))
    return  precio

def leerStock():
    stock = int(input('ingrese stock: '))
    while not(stock >= 0):
        print('error...ingrese un stock positivo')
        stock =int(input('ingrese stock: '))
    return  stock

def leerCodigo():
    os.system('cls')
    print('........Cargar productos........')
    codigo = int(input('ingrese el condigo del nuevo producto: '))
    while not(codigo > 0):
        print('error...ingrese un codigp positivo')
        codigo = int(input('ERROR....ingrese el condigo del nuevo producto: '))
    return codigo 

def leerDescripcion():
    os.system('cls')
    print('......DESCRIPCION......')
    descripcion= str(input('ingrese la descripcion del nuevo producto: '))
    while(descripcion.isspace() or descripcion==''):
        descripcion= str(input('Error....ingrese una descripcion valida: '))
    return  descripcion 

def mostrar(diccionario):
    print('----------Listado de productos----------')
    for clave, valor in diccionario.items():
        print('codigo:[',clave,'] Descripcion:[',valor[0],'] Precio:[$',valor[1],'] Stock:[',valor[2],']')
    print('---------------------------------------------------')


def mostrarsegun(diccionario):
    os.system('cls')
    a=int(input('Ingrese valor inicial del intervalo: ')) 
    b=int(input('Ingrese valor final del intervalo: '))
    print('......Listado de productos cuyo stock esta entre: ','[',a,',',b,'].......')
    for clave, valor in diccionario.items():
        if (valor[2]>=a and valor[2]<=b):
            print('codigo:[',clave,'] Descripcion:[',valor[0],'] Precio:[$',valor[1],'] Stock:[',valor[2],']')

def sumarStock(diccionario):
    os.system('cls')
    print('Ingrese el valor a sumarle al stock: ')
    x=leerStock()
    print('Ingrese valor del stock a comparar: ')
    y=leerStock()
    for clave, valor in diccionario.items():
        if (valor[2]<y):
           valor[2]=valor[2]+x
    print('Se sumo stock correctamente')
    return productos
    
def eliminar(diccionario):
    os.system('cls')
    print('--------------Eliminar PRODUCTOS CON 0 STOCK--------------')    
    elim=[]
    for clave, valor in diccionario.items():
        if (valor[2]==0):
           print('Se va a eliminar: ','codigo:[',clave,'] Descripcion:[',valor[0],'] Precio:[$',valor[1],'] Stock:[',valor[2],']')
           elim.append(clave)
    confirmacion = input('Confirma eliminar el/los producto/s s/n: ')
    if (confirmacion == 's'):
        for i in range(len(elim)):
            del productos[elim[i]]
        os.system('cls')
        print ('Eliminado/s ...')   
    return productos



def IngresarProductos():
    os.system('cls')
    print('........Cargar productos........')
    productos={}
    resp = -1
    while (resp != 0):
        codigo=leerCodigo() 
        if codigo not in productos:    
            descripcion = leerDescripcion()
            precio = leerPrecio()
            stock = leerStock()
            productos[codigo] = [descripcion,precio,stock]
            os.system('cls')
            print('------Producto REGISTRADO correctamente------')
            continuar()

        else:
            print('el codigo de ese producto ya existe')
        os.system('cls')
        resp = int(input('Si desea continuar ingrese [ 1 ](uno) de lo contrario ingrese [ 0 ](cero)'))
    return productos


def salir():
    print('Fin del programa...')


#inicio
opcion = 0

os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = IngresarProductos()
    elif opcion == 2:
        mostrar(productos)
    elif opcion == 3:
        mostrarsegun(productos)
    elif opcion == 4:
        productos = sumarStock(productos)
    elif opcion == 5:
        pruductos=eliminar(productos)
    elif (opcion == 6):     
        os.system('cls')   
        salir()
